from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa

def check(request):
  template = get_template('index.html')
  print('https'  if request.is_secure else 'http')
  params = {'protocol': 'https'  if request.is_secure() else 'http', 'request': request}
  html = template.render(params)
  response = BytesIO()
  pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
  if not pdf.err:
    return HttpResponse(response.getvalue(), content_type = 'application/pdf')
  else :
    return HttpResponse("Error Rendering PDF", status = 400)